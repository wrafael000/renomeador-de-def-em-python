import os
import re
import tkinter as tk
from tkinter import filedialog

def rename():
    dir_path = filedialog.askdirectory(title="Diretório a ser renomeado")
    if not dir_path:
        return

    for root, dirs, files in os.walk(dir_path):
        for file in files:
            if file.endswith(".py"):
                file_path = os.path.join(root, file)
                with open(file_path, "r") as f:
                    code = f.read()

                new_code = []
                for line in code.splitlines():
                    match = re.match(r"def (\w+)\(", line)
                    if match:
                        old_function_name = match.group(1)
                        new_function_name = input("Novo nome para a função {}: ".format(old_function_name))
                        line = line.replace(old_function_name, new_function_name)
                    new_code.append(line)

                with open(file_path, "w") as f:
                    f.write("\n".join(new_code))

root = tk.Tk()
root.title("Renomeador de Funções")

frame = tk.Frame(root)
frame.pack(padx=20, pady=20)

button = tk.Button(frame, text="Renomear Funções", command=rename())
button.pack()

root.mainloop()